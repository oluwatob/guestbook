package com.guestbook.repository;

import com.guestbook.model.GuestBookEntry;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by omitobisam on 3.06.16.
 */
@Repository
public interface GuestbookEntryRepository extends JpaRepository<GuestBookEntry, Long> {
}
