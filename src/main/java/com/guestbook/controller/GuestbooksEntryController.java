package com.guestbook.controller;

import Service.GuestbookEntryService;
import com.guestbook.model.GuestBookEntry;
import com.guestbook.repository.GuestbookEntryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by omitobisam on 3.06.16.
 */
@Controller

public class GuestbooksEntryController {

    @Autowired
    GuestbookEntryRepository guestbookRepo;


    GuestbookEntryService guestbookEntryService = new GuestbookEntryService();

    List<String> comments = new ArrayList<>();


    @RequestMapping(path = "/")
    public String getBooks(Model model){
            model.addAttribute("entries", guestbookRepo.findAll());
        return "books";
    }

    @RequestMapping(value = "/form")
    public String prepareEntry(Model model ){
        model.addAttribute("entry", new GuestBookEntry());
        return "entry";
    }

    @RequestMapping(value= "/", method= RequestMethod.POST)
    public String addEntry(GuestBookEntry guestBookEntry){
        boolean emailValid = guestbookEntryService.validateEmail(guestBookEntry.getEmail());
        //System.out.println(guestBookEntry);
        if (emailValid == true) {
            guestbookRepo.save(guestBookEntry);
            return "redirect:/";
        } else
        {
            //System.out.println(guestBookEntry.getEmail());

            //Invalid Email redirects back to the same form: a second-level validation
            return "redirect:/form";
        }
    }



//    @RequestMapping("/books/{id}/form")
//    public String addComment(Model model, @PathVariable Long id){
//        //TO COMPLETE
//        GuestBookEntry guestBookEntry = guestbookRepo.findOne(id);
//        comments.add(guestBookEntry.getComment());
//        model.addAttribute("comments", comments);
//        return "comment";
//    }
//
//    @RequestMapping(value= "/templates/plants/{id}", method=RequestMethod.PUT)
//    public String update(Plant plant, @PathVariable Long id){
//        //TO COMPLETE
//        //Plant oneplant = this.repo.findOne(id);
//        repo.saveAndFlush(plant);
//        return "redirect:/plants";
//    }
//    @RequestMapping("/books/{id}")
//    public String showOneEntry(Model model, @PathVariable Long id){
//        // TO COMPLETE
//        model.addAttribute("/books", guestbookRepo.findOne(id));
//        return "books";
//
//    }
}
