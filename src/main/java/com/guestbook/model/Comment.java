package com.guestbook.model;

import lombok.Data;

import javax.persistence.Embeddable;
import javax.persistence.Entity;

/**
 * Created by omitobisam on 3.06.16.
 */
@Embeddable
public class Comment {
    String comment;
}
