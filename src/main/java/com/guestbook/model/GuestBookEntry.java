package com.guestbook.model;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

/**
 * Created by omitobisam on 3.06.16.
 */
@Entity
@Data
public class GuestBookEntry {
    @Id
    @GeneratedValue
    Long id;

    String name;
    String email;
    String comment;
}
